HashiCorp Vault + Checkmarx CxFlow - Docker
------------

This project contains resources for building a Docker image based on checkmarx/cx-flow that includes the HashiCorp Vault binary for secrets management.

Usage
------------

There is a helper script which you can run to build and destroy the deployment.

Start or destroy the deployment:

```bash
./start [build|destroy]
```